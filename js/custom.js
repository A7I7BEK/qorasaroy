

/*__________ Loader +++ __________*/
function Loader(delay) {

	$('.loader_box').addClass('active');
	setTimeout(function () {
		$('.loader_box').removeClass('active');
	}, delay);

}
/*__________ Loader --- __________*/

/*__________ Page Loading +++ __________*/
$(window).load(function () {
	// Loader(2000);
})
/*__________ Page Loading --- __________*/

/*__________ Owl Carousel +++ __________*/
$(document).ready(function() {

	$('.baner').owlCarousel({
		// autoPlay : 5000,
		stopOnHover : true,
		navigation: true,
		pagination: false,
		goToFirstSpeed: 2000,
		singleItem: true,
		autoHeight: true,
		transitionStyle: "backSlide"
	});

});

$(document).ready(function() {

	var owl = $(".filter_baner");

	owl.owlCarousel({
		items : 4,
		lazyLoad : true,
		navigation: false,
		pagination: false
	});

	// Custom Navigation Events
	$(".filter_baner_btn.next").click(function(){
		owl.trigger('owl.next');
	})
	$(".filter_baner_btn.prev").click(function(){
		owl.trigger('owl.prev');
	})

});

$(document).ready(function() {

	$(".short_about_baner").owlCarousel({
		// autoPlay : 5000,
		stopOnHover : true,
		navigation: true,
		pagination: false,
		goToFirstSpeed: 2000,
		singleItem: true,
		transitionStyle: "fadeUp"
	});

});

$(document).ready(function() {

	$(".partner_baner").owlCarousel({
		// autoPlay : 5000,
		items : 5,
		lazyLoad : true,
		navigation: true,
		pagination: false
	});

});
/*__________ Owl Carousel --- __________*/




/*__________ Mobile Menu +++ __________*/
$('.nav_menu_btn, .nav_menu_bg').click(function () {
	if ($('.nav_menu_btn').hasClass('active'))
	{
		$('.nav_menu_btn').removeClass('active');
		$('.nav_menu_box').removeClass('active');
		$('.nav_menu_bg').removeClass('active');

		$('html, body').removeClass('over_hidden');
	}
	else {
		$('.nav_menu_btn').addClass('active');
		$('.nav_menu_box').addClass('active');
		$('.nav_menu_bg').addClass('active');

		$('html, body').addClass('over_hidden');
	}
});
/*__________ Mobile Menu --- __________*/



/*__________ Catalog Item +++ __________*/
$('.catalog_menu > li > a').click(function () {

	var i = $(this).parent().index();
	$('.catalog_item_box').eq(i).addClass('active').siblings().removeClass('active');

	$(this).parent().addClass('active').siblings().removeClass('active');

	return false;
});
/*__________ Catalog Item --- __________*/



/*__________ Filter +++ __________*/
$(document).on('click', '.filter_menu_lg > li > a', function () {

	var i = $(this).parent().index();
	$('.filter_item_box_lg').eq(i).addClass('active').siblings().removeClass('active');

	$(this).parent().addClass('active').siblings().removeClass('active');

	var delay = 3000;
	$('.filter_item_box_lg').css('animation-delay', delay + 'ms');
	Loader(delay);

	return false;
});


$(document).on('click', '.filter_menu_sm > li:not(.more) > a', function () {

	var i = $(this).parent().index();
	$(this).closest('.filter_item_box_lg').find('.filter_item_box_sm').eq(i).addClass('active').siblings().removeClass('active');

	$(this).parent().addClass('active').siblings().removeClass('active');

	var delay = 3000;
	$('.filter_item_box_sm').css('animation-delay', delay + 'ms');
	Loader(delay);

	return false;
});
/*__________ Filter --- __________*/




/*__________ Like Button +++ __________*/
$('.blog_item_like > i').click(function () {

	var likeNumber = parseInt($(this).siblings('.number').text());

	if ($(this).hasClass('active'))
	{
		$(this).removeClass('active');

		$(this).siblings('.number').text(likeNumber - 1)
	}
	else
	{
		$(this).addClass('active');

		$(this).siblings('.number').text(likeNumber + 1)
	}
});
/*__________ Like Button --- __________*/





/*__________ Form Control +++ __________*/
$('.delivery_form_control > input').click(function () {
	$('.delivery_form_control').removeClass('active');
	$(this).parent().addClass('active');
});

$(document).click(function (e){
	if ($(e.target).is(".delivery_form_control > input") === false)
	{
		$('.delivery_form_control').removeClass('active');
	}
});
/*__________ Form Control +++ __________*/







/*__________ Catalog Alone +++ __________*/
$('.ctlg_alone_menu > li > a').click(function () {
	$(this).next('.ctlg_alone_drop_menu').toggleClass('active');

	return false;
});


$('.ctlg_alone_drop_menu > li > a').click(function () {
	$('.ctlg_alone_menu li').removeClass('active');

	$(this).parents('li').addClass('active');
});
/*__________ Catalog Alone --- __________*/





/*__________ Validation +++ __________*/
$('.call_back_button.app').click(function (e) {
	e.preventDefault();
	$('.call_back_app').removeClass('active');
	$('.call_back_rcvd').addClass('active');
});
/*__________ Validation +++ __________*/








/*__________ Validation +++ __________*/


